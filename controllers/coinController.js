import { userModel } from '../models/nedbModel.js';
import CoinGecko from 'coingecko-api';

/**
 * @api {get} /coins/all Listar criptomonedas disponibles
 * @apiVersion 0.1.0
 * @apiName All
 * @apiGroup Coins
 *
 * @apiParam {String} token  Token
 *
 * @apiSuccess {Array}  coins   Listado de criptomonedas 
 * 
 */

const all = (req, res) => {
    const code = req.user.prefCurrency;
    const CoinGeckoClient = new CoinGecko();
    CoinGeckoClient.coins.all().then((data) => {
        const coins = []
        data.data.forEach(e => {
            coins.push({
                symbol: e.symbol,
                price: e.market_data.current_price[code],
                name: e.name,
                image: e.image,
                last_updated: e.last_updated,
            })
        });
        
        res.send(coins);
    }).catch((e) => res.send(e));
};

/**
 * @api {put} /coins/add/:currency Agregar moneda a lista de favoritas
 * @apiVersion 0.1.0
 * @apiName Add
 * @apiGroup Coins
 *
 * @apiParam {String} currency  Simbolo de la criptomoneda a agregar - ejemplo: btc
 * @apiParam {String} token  Token
 *
 * @apiSuccess {String} ok
 * 
 */
const add = async (req, res) => {
    const {currency} = req.params;
    const {user} = req;
    const CoinGeckoClient = new CoinGecko();

    if(!currency){
        res.status(400).send("Invalid request");
    }
    
    if (user.fCoin.find(e => e.currency === currency)){
        res.status(400).send("Currency alredy added");
    }else{
        try{
            const coins_available = await CoinGeckoClient.coins.list();
            const coin_info = coins_available.data.find(e => e.symbol === currency ? e : undefined);
            if(coin_info){
                user.fCoin.push({currency, id: coin_info.id});
                delete user._id;
                await userModel.update({username: user.username}, {...user});
                res.status(202).send("ok");
           }else{
                res.status(400).send("Symbol currency invalid");   
           }
        }catch{
            res.status(500).send("ok");
        }
    }
};

/**
 * @api {get} /coins/top/:N Top N de criptomonedas favoritas
 * @apiVersion 0.1.0
 * @apiName top
 * @apiGroup Coins
 *
 * @apiParam {String} token  Token
 * @apiParam {String} N Cantidad de registros a obtener (menor o igual que 25)
 * @apiParam {String} Direccion a ordenar (asc o desc)
 *
 * @apiSuccess {Array} Top  Top N
 * 
 */

const top = async (req, res) => {
    const CoinGeckoClient = new CoinGecko();
    const {fCoin} = req.user;

    if(req.params.n > 25 ){
        res.status(400).send("Max N is 25");
    }

    const coins_promise = fCoin.map(e => {
        return CoinGeckoClient.coins.fetch(e.id).then(data => {
            return {
                symbol: data.data.symbol,
                price: {
                    ars: data.data.market_data.current_price['ars'],
                    usd: data.data.market_data.current_price['usd'],
                    eur: data.data.market_data.current_price['eur'],
                },
                name: data.data.name,
                last_updated: data.data.market_data.last_updated,
            }
            
        });
    })

    const coins = await Promise.all(coins_promise)

    let coins_s = [];

    if(req.query.dir === 'asc'){
         coins_s = coins.sort((a, b) => {
            return a.price[req.user.prefCurrency] - b.price[req.user.prefCurrency];
        })
    }else{
        coins_s = coins.sort((a, b) => {
            return b.price[req.user.prefCurrency] - a.price[req.user.prefCurrency];
        })
    }

    res.send(coins_s.slice(0, req.params.n));
};

export default {
    all,
    add,
    top,
};