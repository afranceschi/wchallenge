import { userModel } from "../models/nedbModel.js";
import bcrypt from 'bcrypt';
import { privateKey } from "../config.js";
import jwt from "jsonwebtoken";

/**
 * @api {post} /users/login Login de usuarios
 * @apiVersion 0.1.0
 * @apiName Login
 * @apiGroup Users
 *
 * @apiParam {String} Username  Nombre de usuario
 * @apiParam {String} Password  Password
 *
 * @apiSuccess {String} id      User ID
 * @apiSuccess {String} token   Token
 * 
 */

const login = async (req, res) => {
    const {username, password} = req.body;
    if (!username || !password) {
        res.status(400).send("All input is required");
    }else{
        const user = await userModel.read({username});
        if(user.length > 0){
            if(await bcrypt.compare(password, user[0].password)){
                const token = jwt.sign({id: user[0]._id, username}, privateKey,{ expiresIn: '1h'});
                res.send({id: user[0]._id, token});
            }else{
                res.status(400).send("Invalid Credentials");
            };
        }else{
            res.status(400).send("Invalid Credentials");
        }
        
    }

};

/**
 * @api {post} /users/create Crear usuarios
 * @apiVersion 0.1.0
 * @apiName Create
 * @apiGroup Users
 *
 * @apiParam {String} firstName Nombre
 * @apiParam {String} lastName  Apellido
 * @apiParam {String} username  Nombre de usuario
 * @apiParam {String} password  Password
 * @apiParam {String} prefCurrenci  Moneda preferida (ars, usd, eur)
 *
 * @apiSuccess {String} id      User ID
 * @apiSuccess {String} token   Token
 * 
 */
const create = async (req, res) => {
    const {firstName, lastName, username, password, prefCurrency} = req.body;
    const usernameExist = (await userModel.read({username})).length ? true : false;
    const validCurrency = ['eur', 'usd', 'ars']

    if(!validCurrency.includes(prefCurrency)){
        res.status(400).send('Preferred currency not supported.');
    }else if(usernameExist){
        res.status(409).send('Username alredy exist.');
    }else{
        if(password){
            const encPassword = await bcrypt.hash(password, 10);
            userModel.create({
                firstName,
                lastName,
                username,
                password: encPassword,
                prefCurrency,
                fCoin: [],
            }).then((data) => {
                const token = jwt.sign({
                    userId: data._id, username
                }, privateKey, {expiresIn: '1h'});
                res.status(201).send({id: data._id, token})
            }).catch((e) => {
                res.status(400).send(e.message);
            });
        }else{
            res.status(400).send("password (undefined): Property 'password' is required")
        }
    }
};

export default {
    login,
    create,
}