import { model, adapter, use } from 'modli';
import Joi from 'joi';
import nedb from 'modli-nedb';
import path, { join } from 'path'

const dbPath = path.resolve('db');

adapter.add({
  name: 'challengeDB',
  source: nedb,
  config: {
    filename: dbPath + '/user.db',
    autoload: true,
  }
});

model.add({
  name: 'userModel',
  tableName: 'userModel',
  version: 1,
  schema: {
    firstName: Joi.string(),
    lastName: Joi.string(),
    username: Joi.string(),
    password: Joi.string(),
    prefCurrency: Joi.string(),
    fCoin: Joi.array().unique().default([]),
  },
});

export const userModel = use('userModel', 'challengeDB');
