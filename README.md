## Instalacion

`$ git clone https://gitlab.com/afranceschi/wchallenge.git`

`$ cd wchallenge`

`$ npm install`


## Uso

Se debe ejecutar con el comando:
`npm start`

Una vez iniciada, la api se encuentra disponible en 
`http://localhost:3000`

## Documentacion

La documentacion de la api se encuntra en la carpeta doc.

## Comentarios

Database: Se opto por el uso de neDB como base de datos unicamente porque se trata de una prueba y se evita la complicacion en la instalacion y ejecucion. Es recomendable migrar a una base de datos como MongoDB en caso de ser necesario escalar el proyecto. Por tal razon se utilizo Modli como modelador de datos para poder cambiar de una forma simple.