import express from 'express';
import {checkToken} from '../middlewares/authentication.js';

var router = express.Router();


import coinController from '../controllers/coinController.js';

router.get('/all', checkToken, coinController.all);
router.get('/top/:n', checkToken, coinController.top);
router.put('/add/:currency', checkToken, coinController.add);

export default router;