import express from 'express';
import { userModel } from '../models/nedbModel.js';
import userController from '../controllers/userController.js';
import {checkToken} from '../middlewares/authentication.js';

var router = express.Router();

router.post('/login', userController.login);

router.post('/create', userController.create);

export default router;