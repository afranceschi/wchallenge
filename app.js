import express from 'express';
import userRoutes from './routes/userRoutes.js';
import coinRoutes from './routes/coinRoutes.js';

const app = express();

app.use(express.json());

app.use('/users', userRoutes);
app.use('/coins', coinRoutes);

app.listen(3000, () => {console.log("Servicio iniciado en http://localhost:3000")})