import jwt from 'jsonwebtoken';
import { privateKey } from '../config.js';
import { userModel } from '../models/nedbModel.js';

export const checkToken = async (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['access-token'];

    if(!token){
        return res.status(403).send('Forbidden. Login first');
    }

    try{
        const userTokenData = jwt.verify(token, privateKey);
        req.user = (await userModel.read({username: userTokenData.username}))[0]
    } catch(err) {
        return res.status(403).send('Token error');
    }

    return next();

}